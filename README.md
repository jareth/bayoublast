#Bayou Blast
A Three.js and WebGL Experiement

## Goal
The goal of this project is to port to WebGL a game I made previously for Flash and iOS. The game uses trigonometric functions to generate terrain data so should be a good opportunity to explore shader implementation in WebGL.

A [demo](http://jareth.vanbone.com/bayou) of this project is include in my [portfolio site](http://jareth.vanbone.com).

##Build (complete site with Express server)
### Requirements
* [Node.js](http://nodejs.org/) (v0.10.22)
* [Bower](http://bower.io/) (v1.3.1)

### Steps
1. Download and extract the source files from the [BitBucket repository](https://bitbucket.org/jareth/bayoublast/get/master.zip).
2. Navigate to the root directory and run `npm install` to install node dependencies.
3. Navigate to the www directory and run `bower install` to install the bower dependencies.
4. Navigate to the root directory and run `node coffeeBootstrap.js` to start the web server.
5. Load up the web address displayed in the console in a webgl supporting browser. (local servers will be `127.0.0.1:3000`)

##Contact
[@jareth](https://bitbucket.org/jareth) : [Jareth van Bone](http://jareth.vanbone.com)

##License
Copyright (c) 2014 Jareth van Bone

Licensed under the MIT License

Code licensed under the [AGPL-3.0 License](http://opensource.org/licenses/AGPL-3.0).