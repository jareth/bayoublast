module.exports = (grunt)->

  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')

    coffeelint:
      options:
        max_line_length:
          level: 'ignore'
      app: ['app/**/*.coffee', 'gruntfile.coffee']

    coffee:
      compile:
        options:
          join: true
        files:
          'www/js/bayou.js': [
            'app/bayoublast/**/*.coffee'
          ],
          'www/js/app.js': [
            'app/site/app.coffee',
            'app/namespace.coffee'
          ]

    jshint:
      options: {
        eqnull: true
        shadow: false
        ignores: ['www/js/**/*.min.js']
      },
      all: ['www/js/**/*.js', 'www/javascripts/**/*.js']

    uglify:
      options:
        compress:
          drop_console: true
      app:
        files:
          'www/js/app.min.js': ['www/js/app.js']
          'www/js/bayou.min.js': ['www/js/bayou.js']

    sass:
      dist:
        options:
          #includePaths: ['www/bower_components/foundation/scss']
          outputStyle: 'compressed'
        files:
          'www/css/app.css': 'app/app.scss'

    express:
      coffee:
        options:
          script: 'coffeeBootstrap.js'
          #cmd: 'coffee'
          port: process.env.PORT

    watch:
      grunt: 'gruntfile.coffee'

      sass:
        files: ['app/**/*.scss']
        tasks: ['sass']

      express:
        files: ['app/**/*.coffee']
        tasks: ['coffeelint', 'coffee', 'express:coffee']
        options:
          spawn: false

    codo:
      options:
        title: 'Bayou Blast'
        output: 'www/doc'
        inputs: ['app/bayoublast']


  grunt.loadNpmTasks 'grunt-coffeelint'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-jshint'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-sass'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-express-server'
  grunt.loadNpmTasks 'grunt-codo'

  grunt.registerTask 'prod', ['build', 'uglify', 'codo']
  grunt.registerTask 'dev', ['build', 'express:coffee', 'watch']
  grunt.registerTask 'build', ['coffeelint', 'coffee', 'sass']
  grunt.registerTask 'default', ['dev']
