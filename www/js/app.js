(function() {
  var changeSelection, choose, initKeyboardEvents, initMenuEvents, selectionDown, selectionUp, toggleMenu;

  $(function() {
    initMenuEvents();
    return initKeyboardEvents();
  });

  initMenuEvents = function() {
    $('#toggleMenu').click(toggleMenu);
    $('#resetStreams').click(function() {
      return window.app.resetPosition();
    });
    $('#seedGenerate').click(function() {
      return window.app.generate($('#seedEntry').val());
    });
    $('#switchCamera').click(function() {
      return window.app.switchCamera();
    });
    $('#arcadeMode').click(function() {
      var $container, error;
      try {
        $('#start').toggle(false);
        $container = $('#container');
        $container.toggle(true);
        window.app = new Main($container.get(0), $('#timer').get(0));
        return window.app.animate(0);
      } catch (_error) {
        error = _error;
        return console.log('Creating 3d scene failed.', error);
      }
    });
    $('#settings').click(function() {
      $('#mainMenu').toggle(false);
      $('#settingsMenu').toggle(true);
      return changeSelection($('#settingsMenu').children(':first'));
    });
    return $('#closeSettings').click(function() {
      $('#mainMenu').toggle(true);
      $('#settingsMenu').toggle(false);
      return changeSelection($('#settings').parent());
    });
  };

  initKeyboardEvents = function() {
    return $(document).keydown(function(event) {
      switch (event.which) {
        case 27:
          return toggleMenu();
        case 38:
          return selectionUp();
        case 40:
          return selectionDown();
        case 13:
        case 32:
          return choose();
        default:
          return console.log(event.which);
      }
    });
  };

  toggleMenu = function() {
    var isOpening;
    isOpening = !$('#settingsMenu').is(':visible');
    $('#start').toggle(isOpening);
    $('#mainMenu').toggle(!isOpening);
    $('#settingsMenu').toggle(isOpening);
    return window.app.togglePause(isOpening);
  };

  selectionUp = function() {
    var $current, $next;
    $current = $('.menu .selected');
    $next = $current.prev().length !== 0 ? $current.prev() : $current.siblings(':last');
    return changeSelection($next);
  };

  selectionDown = function() {
    var $current, $prev;
    $current = $('.menu .selected');
    $prev = $current.next().length !== 0 ? $current.next() : $current.siblings(':first');
    return changeSelection($prev);
  };

  choose = function() {
    var $current;
    $current = $('.menu .selected');
    return $current.children('a').click();
  };

  changeSelection = function($target) {
    var $current;
    $current = $('.menu .selected');
    $current.removeClass('selected');
    return $target.addClass('selected');
  };

  /*
  # Elegant pattern to simulate namespace in CoffeeScript
  #
  # @author Maks
  */


  (function(root) {
    var fn;
    fn = function() {
      var Class, args, name, obj, subpackage, target;
      args = arguments[0];
      target = root;
      while (true) {
        for (subpackage in args) {
          obj = args[subpackage];
          target = target[subpackage] || (target[subpackage] = {});
          args = obj;
        }
        if (typeof args !== 'object') {
          break;
        }
      }
      Class = args;
      if (arguments[0].hasOwnProperty('global')) {
        target = root;
      }
      name = Class.toString().match(/^function\s(\w+)\(/)[1];
      target[name] = Class;
    };
    root.namespace = fn;
    root.module = fn;
  })(typeof global !== "undefined" && global !== null ? global : window);

}).call(this);
