(function() {
  var AirState, Boat, BoatState, Detector, Ground, GroundShader, GroundState, Sky, SkyShader, Streams, Water, WaterShader, WaterShaderOld, WaterState,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Boat = (function() {
    Boat.prototype.state = null;

    function Boat(scene) {
      var loader,
        _this = this;
      this.state = new WaterState(null);
      this.state.context = this;
      this.position = new THREE.Vector3(0, 0, 0);
      loader = new THREE.JSONLoader();
      loader.load('/js/boat1.js', function(geometry, materials) {
        _this.material = new THREE.MeshFaceMaterial(materials);
        _this.mesh = new THREE.Mesh(geometry, _this.material);
        _this.mesh.scale.set(0.4, 0.4, 0.4);
        _this.mesh.rotation.x = Math.PI / 2;
        _this.mesh.rotation.y = Math.PI / 2;
        _this.mesh.position.z = _this.position.clone();
        return scene.add(_this.mesh);
      });
    }

    Boat.prototype.getMesh = function() {
      return this.mesh;
    };

    Boat.prototype.update = function(speed) {
      this.position.x -= this.state.angle * 1500 * this.state.speed;
      this.position.z = 10 + this.state.height;
      if (this.mesh != null) {
        this.mesh.position.x = this.position.x;
        this.mesh.position.z = this.position.z;
        this.mesh.rotation.y = Math.PI / 2 + this.state.angle;
      }
    };

    Boat.prototype.getPosition = function() {
      return this.position.clone();
    };

    Boat.prototype.getSpeed = function() {
      return this.state.getSpeed();
    };

    Boat.prototype.turnLeft = function() {
      return this.state.turnLeft();
    };

    Boat.prototype.turnRight = function() {
      return this.state.turnRight();
    };

    Boat.prototype.collide = function(hit) {
      return this.state.collide(hit);
    };

    Boat.prototype.resetPosition = function(widest) {
      this.position = new THREE.Vector3(widest, 0, 0);
      if (typeof mesh !== "undefined" && mesh !== null) {
        this.mesh.position = this.position.clone();
      }
      return this.state.reset();
    };

    Boat.prototype.turnToAngle = function(targetAngle) {
      return this.state.turnToAngle(targetAngle);
    };

    return Boat;

  })();

  Detector = (function() {
    var canvas, error;

    function Detector() {}

    Detector.canvas = window.CanvasRenderingContext2D != null;

    Detector.webgl = ((function() {
      try {
        canvas = document.createElement('canvas');
        if ((window.WebGLRenderingContext != null) && (((canvas.getContext('webgl')) != null) || ((canvas.getContext('experimental-webgl')) != null))) {
          return true;
        }
      } catch (_error) {
        error = _error;
        return false;
      }
    })());

    Detector.workers = !!window.Worker;

    Detector.fileapi = (window.File != null) && (window.FileReader != null) && (window.FileList != null) && (window.Blob != null);

    Detector.deviceMotion = !!window.DeviceMotionEvent;

    Detector.deviceOrientation = !!window.DeviceOrientationEvent;

    return Detector;

  })();

  Ground = (function() {
    Ground.prototype.distance = 0.0;

    function Ground(width, length, streams, density) {
      var geometry;
      this.streams = streams;
      if (density == null) {
        density = 100;
      }
      geometry = new THREE.PlaneBufferGeometry(width, length, density, density);
      this.material = new THREE.ShaderMaterial({
        uniforms: {
          time: {
            type: "f",
            value: 0.0
          },
          scale: {
            type: "f",
            value: 10.0
          },
          streams: {
            type: "v4v",
            value: streams.getUniformArray()
          },
          streamCount: {
            type: "i",
            value: streams.getStreamCount()
          }
        },
        vertexShader: GroundShader.vertexShader,
        fragmentShader: GroundShader.fragmentShader
      });
      this.material.transparent = true;
      this.mesh = new THREE.Mesh(geometry, this.material);
    }

    Ground.prototype.getMesh = function() {
      return this.mesh;
    };

    Ground.prototype.update = function(speed) {
      this.distance += speed;
      return this.material.uniforms.time.value = this.distance;
    };

    Ground.prototype.resetPosition = function() {
      this.distance = 0.0;
      this.material.uniforms.time.value = this.distance;
      return this.material.uniforms.streams.value = this.streams.getUniformArray();
    };

    return Ground;

  })();

  GroundShader = (function() {
    function GroundShader() {}

    GroundShader.uniforms = {
      time: {
        type: "f",
        value: 0.0
      },
      scale: {
        type: "f",
        value: 1.0
      }
    };

    GroundShader.vertexShader = "// pass UV coords to the fragment shader\nvarying vec2 vUv;\n\n// pass height to fragment shader\nvarying float fHeight;\n\n// height of landscape\nuniform float scale;\n\n// animation ticker\nuniform float time;\n\n// stream uniforms\nuniform vec4 streams[10];\nuniform int streamCount;\n\n// stream1\n\n// calculate ground height for position: 0 = water, 1 = land\nfloat renderStream( vec2 p, float freq, float phase, float amp, float xOff) {\n  float h = 0.0;\n  float s1 = cos( (p.y + phase + time) * freq ) / amp + 0.5 - xOff;\n  float w1 = abs(( cos( (p.y + phase + time) * (freq / 7.0)) + 0.5) / (amp * 2.0));\n  if (p.x > s1) {\n    h = p.x - (s1 + w1);\n  } else if (p.x < s1) {\n    h = (s1 - w1) - p.x;\n  }\n  h = min(max(h * 20.0, 0.0) , 1.0);\n  return h;\n}\n\nfloat renderMap( vec2 p ) {\n  float h = 1.0;\n  for (int iii = 0; iii < 10; iii++) {\n    if (iii < streamCount) {\n      h *= renderStream( p, streams[iii].r, streams[iii].g, streams[iii].b, streams[iii].a);\n    }\n  }\n  return h;\n}\n\n// vertex shader main\nvoid main() {\n\n  // dump the UV coords into the varying (for the fragment shader to use)\n  vUv = uv;\n\n  // calculate terrain height\n  fHeight = renderMap(vUv);\n\n  // modify vertix position based on terrain height\n  gl_Position = projectionMatrix * modelViewMatrix * vec4( position.xy, fHeight * scale, 1.0 );\n}";

    GroundShader.fragmentShader = "// UV coords from the vertex shader\nvarying vec2 vUv;\n\n// height from vertex shader\nvarying float fHeight;\n\n// animation ticker\nuniform float time;\n\n// fragment shader main\nvoid main() {\n\n  // output color blend based on height\n  gl_FragColor = mix(\n    mix(\n      vec4(0.2, 0.9, 0.7, 1.0),\n      vec4(0.7, 0.5 - fHeight * 0.3, 0.3 - fHeight * 0.2, 1.0),\n      smoothstep(fract((vUv.y + time) * 20.0), 0.0, 0.005)\n    ),\n    vec4(0.2, 0.9, 0.7, 1.0),\n    step(0.99, fract(vUv.x * 10.0))\n  );\n  gl_FragColor.a = floor(fHeight + 0.8);\n}";

    return GroundShader;

  })();

  this.Main = (function() {
    Main.prototype.distance = 0.0;

    Main.prototype.isPaused = false;

    Main.prototype.isFpsCamera = true;

    Main.prototype.currentOrientation = -1;

    Main.prototype.lastTime = 0.0;

    Main.prototype.remainingTime = 30000.0;

    Main.prototype.nextCheckpoint = 6;

    function Main(domElement, timerElement) {
      var ambientLight, light;
      this.timerElement = timerElement;
      this.deviceOrientationHandler = __bind(this.deviceOrientationHandler, this);
      this.animate = __bind(this.animate, this);
      this.onWindowResize = __bind(this.onWindowResize, this);
      if (!Detector.webgl) {
        domElement.innerHTML = '<p> WebGL not supported. </p>';
        this.animate = function() {
          return console.log("animate was overriden because WebGL/Canvas not supported.");
        };
        throw new Error("WebGL or Canvas not supported.");
      }
      this.stats = new Stats();
      this.stats.setMode(1);
      document.body.appendChild(this.stats.domElement);
      this.scene = new THREE.Scene();
      this.camera = new THREE.PerspectiveCamera(100, domElement.clientWidth / domElement.clientHeight, 0.1, 10000);
      this.renderer = new THREE.WebGLRenderer();
      this.renderer.setSize(domElement.clientWidth, domElement.clientHeight);
      while (domElement.firstChild) {
        domElement.removeChild(domElement.firstChild);
      }
      domElement.appendChild(this.renderer.domElement);
      this.streams = new Streams(Date(), 5);
      this.sky = new Sky(17000, 4000);
      this.sky.getMesh().position.y = 4000;
      this.sky.getMesh().position.z = 1000;
      this.scene.add(this.sky.getMesh());
      this.ground = new Ground(500, 2000, this.streams, 50);
      this.ground.getMesh().position.y = 900;
      this.scene.add(this.ground.getMesh());
      this.farground = new Ground(500, 2000, this.streams, 30);
      this.farground.getMesh().position.y = 2900;
      this.scene.add(this.farground.getMesh());
      this.farground.update(1);
      this.water = new Water(500, 4000, 0x0000FF);
      this.water.getMesh().position.y = 1900;
      this.water.getMesh().position.z = -5;
      this.scene.add(this.water.getMesh());
      this.boat = new Boat(this.scene);
      this.boat.resetPosition(this.streams.getCenterOfWidestStream() * 500 - 250);
      light = new THREE.PointLight(0xffffff);
      light.position.set(-100, 200, 100);
      this.scene.add(light);
      ambientLight = new THREE.AmbientLight(0x111111);
      this.scene.add(ambientLight);
      this.camera.position.y = -40;
      this.camera.position.z = 150;
      this.camera.lookAt(new THREE.Vector3(0, 50, 0));
      this.keyboard = new THREEx.KeyboardState();
      this.domElement = domElement;
      window.addEventListener('resize', this.onWindowResize, false);
      this.onWindowResize();
      if (Detector.deviceOrientation) {
        console.log('orientation supported');
        window.addEventListener('deviceorientation', this.deviceOrientationHandler, false);
      }
    }

    Main.prototype.render = function() {
      this.scene.updateMatrixWorld();
      this.renderer.clear();
      return this.renderer.render(this.scene, this.camera);
    };

    Main.prototype.onWindowResize = function() {
      this.camera.aspect = this.domElement.clientWidth / this.domElement.clientHeight;
      this.camera.updateProjectionMatrix();
      return this.renderer.setSize(this.domElement.clientWidth, this.domElement.clientHeight);
    };

    Main.prototype.animate = function(thisTime) {
      var height, point, rotValue, speed, targetAngle, timeDelta;
      if (this.isPaused || this.remainingTime <= 0) {
        this.lastTime = thisTime;
        requestAnimationFrame(this.animate);
        return;
      }
      if (this.distance > this.nextCheckpoint) {
        this.nextCheckpoint += 6;
        this.remainingTime += 10000;
      }
      if (this.lastTime === 0) {
        this.lastTime = thisTime;
      } else {
        timeDelta = thisTime - this.lastTime;
        this.remainingTime -= timeDelta;
        this.lastTime = thisTime;
        this.timerElement.innerHTML = this.pad(Math.ceil(this.remainingTime * 0.001), 2, '0');
      }
      this.stats.begin();
      speed = this.boat.getSpeed();
      this.isPaused = speed === 0;
      if (this.keyboard.pressed("left")) {
        this.boat.turnLeft();
      }
      if (this.keyboard.pressed("right")) {
        this.boat.turnRight();
      }
      if (this.currentOrientation !== -1) {
        rotValue = 0;
        if (this.currentOrientation === 0) {
          rotValue = this.gamma;
        } else if (this.currentOrientation === 90) {
          rotValue = this.beta;
        } else if (this.currentOrientation === -90) {
          rotValue = -this.beta;
        }
        if (rotValue < -1 || rotValue > 1) {
          targetAngle = -rotValue * Math.PI / 180;
          this.boat.turnToAngle(targetAngle);
        }
      }
      this.distance += speed;
      this.streams.update(speed);
      this.ground.update(speed);
      this.farground.update(speed);
      this.water.update(speed);
      this.boat.update(speed);
      this.sky.update(speed);
      point = this.boat.getPosition();
      point.x = (point.x + 250) / 500;
      point.y = 100 / 2000;
      height = this.streams.checkCollision(point);
      this.boat.collide(height);
      if (this.isFpsCamera) {
        this.camera.position.y = -10;
        this.camera.position.z = 20 + this.boat.getPosition().z + Math.sin(this.distance);
      } else {
        this.camera.position.y = -40;
        this.camera.position.z = 150 - (speed * 3000);
      }
      this.camera.position.x = this.boat.getPosition().x;
      this.camera.lookAt(new THREE.Vector3(this.boat.getPosition().x, 50, this.boat.getPosition().z));
      this.render();
      requestAnimationFrame(this.animate);
      return this.stats.end();
    };

    Main.prototype.togglePause = function(isPausing) {
      return this.isPaused = isPausing;
    };

    Main.prototype.resetPosition = function() {
      this.distance = 0.0;
      this.streams.resetPosition();
      this.ground.resetPosition();
      this.farground.resetPosition();
      this.boat.resetPosition(this.streams.getCenterOfWidestStream() * 500 - 250);
      this.farground.update(1);
      this.lastTime = 0;
      this.remainingTime = 30000.0;
      this.nextCheckpoint = 6;
      return this.render();
    };

    Main.prototype.generate = function(seed) {
      this.streams.generate(seed, 5);
      return this.resetPosition();
    };

    Main.prototype.deviceOrientationHandler = function(eventData) {
      this.gamma = eventData.gamma;
      this.beta = eventData.beta;
      return this.currentOrientation = window.orientation;
    };

    Main.prototype.switchCamera = function() {
      return this.isFpsCamera = !this.isFpsCamera;
    };

    Main.prototype.pad = function(val, length, padChar) {
      var numPads;
      if (padChar == null) {
        padChar = '0';
      }
      val += '';
      numPads = length - val.length;
      if (numPads > 0) {
        return new Array(numPads + 1).join(padChar) + val;
      } else {
        return val;
      }
    };

    return Main;

  })();

  Sky = (function() {
    Sky.prototype.distance = 0.0;

    Sky.prototype.stepper = 0.0;

    function Sky(width, length) {
      var geometry;
      geometry = new THREE.PlaneBufferGeometry(width, length, 100, 100);
      this.material = new THREE.ShaderMaterial({
        uniforms: {
          time: {
            type: "f",
            value: 0.0
          }
        },
        vertexShader: SkyShader.vertexShader,
        fragmentShader: SkyShader.fragmentShader
      });
      this.mesh = new THREE.Mesh(geometry, this.material);
      this.mesh.position.z = 0;
      this.mesh.rotation.x = Math.PI / 2;
    }

    Sky.prototype.getMesh = function() {
      return this.mesh;
    };

    Sky.prototype.update = function(speed) {
      this.stepper += 1;
      if (this.stepper % 30 === 0) {
        this.distance += speed;
        return this.material.uniforms.time.value = this.distance;
      }
    };

    return Sky;

  })();

  SkyShader = (function() {
    function SkyShader() {}

    SkyShader.uniforms = {
      time: {
        type: "f",
        value: 0.0
      }
    };

    SkyShader.vertexShader = "// pass UV coords to the fragment shader\nvarying vec2 vUv;\n\n// vertex shader main\nvoid main() {\n\n  // dump the UV coords into the varying (for the fragment shader to use)\n  vUv = uv;\n\n  // modify vertix position based on terrain height\n  gl_Position = projectionMatrix * modelViewMatrix * vec4( position.xyz, 1.0 );\n}";

    SkyShader.fragmentShader = "// UV coords from the vertex shader\nvarying vec2 vUv;\n\n// animation ticker\nuniform float time;\n\nhighp float rand(vec2 co)\n{\n  highp float a = 12.9898;\n  highp float b = 78.233;\n  highp float c = 43758.5453;\n  highp float dt= dot(co.xy ,vec2(a,b));\n  highp float sn= mod(dt,3.14);\n  return fract(sin(sn) * c);\n}\n\nfloat myRound(float a) {\n  return floor(a * 100.0)/100.0;\n}\n\n// fragment shader main\nvoid main() {\n  float sparkle = rand(vec2(myRound(vUv.x), myRound(vUv.y - time * 20.0))) * 0.05;\n  vec2 center = vUv - vec2(0.5, 0.25);\n  float dist = 1.0 - sqrt(dot(center, center));\n\n  // output solid color\n  gl_FragColor = vec4(dist + sparkle, max(dist - 0.6, sparkle) * 1.8, max(0.9 - sparkle - dist, 0.0) * 2.0, 1.0);\n  // gl_FragColor = mix(\n  //  vec4(sparkle, 0.0, 0.0, 1.0),\n  //  vec4(1.0, 1.0, 1.0, 1.0),\n  //  step(dist, 0.1)\n  //);\n}";

    return SkyShader;

  })();

  Streams = (function() {
    Streams.prototype.distance = 0.0;

    function Streams(seed, num) {
      this.num = num;
      this.generate(seed, num);
    }

    Streams.prototype.generate = function(seed, num) {
      var iii, _i, _results;
      this.num = num;
      this.streams = [];
      Math.seedrandom(seed);
      _results = [];
      for (iii = _i = 1; 1 <= num ? _i <= num : _i >= num; iii = 1 <= num ? ++_i : --_i) {
        _results.push(this.streams.push(new THREE.Vector4((Math.random() * 4) + 3, (Math.random() * 10) + 0, (Math.random() * 2) + 10, (Math.random() * 0.6) - 0.3)));
      }
      return _results;
    };

    Streams.prototype.getUniformArray = function() {
      return this.streams;
    };

    Streams.prototype.getStreamCount = function() {
      return this.num;
    };

    Streams.prototype.update = function(speed) {
      this.distance += speed;
    };

    Streams.prototype.checkCollision = function(point) {
      var height, stream, _i, _len, _ref;
      height = 1.0;
      _ref = this.streams;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        stream = _ref[_i];
        height *= this.getHeight(point, stream.x, stream.y, stream.z, stream.w);
      }
      return height;
    };

    Streams.prototype.getHeight = function(point, freq, phase, amp, xOff) {
      var center, height, width;
      height = 1.0;
      center = Math.cos((point.y + phase + this.distance) * freq) / amp + 0.5 - xOff;
      width = Math.abs((Math.cos((point.y + phase + this.distance) * (freq / 7.0)) + 0.5) / (amp * 2.0));
      if (point.x > center) {
        height = point.x - (center + width);
      } else if (point.x < center) {
        height = (center - width) - point.x;
      }
      height = Math.min(Math.max(height * 20.0, 0.0), 1.0);
      return height;
    };

    Streams.prototype.resetPosition = function() {
      return this.distance = 0.0;
    };

    Streams.prototype.getCenterOfWidestStream = function() {
      var center, centerAndWidth, stream, widest, _i, _len, _ref;
      widest = 0;
      center = 0.5;
      _ref = this.streams;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        stream = _ref[_i];
        centerAndWidth = this.getWidth(stream.x, stream.y, stream.z, stream.w);
        console.log(centerAndWidth);
        if (centerAndWidth[1] > widest) {
          widest = centerAndWidth[1];
          center = centerAndWidth[0];
        }
      }
      return center;
    };

    Streams.prototype.getWidth = function(freq, phase, amp, xOff) {
      var center, width;
      center = Math.cos((phase + this.distance) * freq) / amp + 0.5 - xOff;
      width = Math.abs((Math.cos((phase + this.distance) * (freq / 7.0)) + 0.5) / (amp * 2.0));
      return [center, width];
    };

    return Streams;

  })();

  Water = (function() {
    Water.prototype.distance = 0.0;

    function Water(width, length, color) {
      var geometry;
      geometry = new THREE.PlaneBufferGeometry(width, length, 100, 100);
      this.material = new THREE.ShaderMaterial({
        uniforms: {
          time: {
            type: "f",
            value: 0.0
          }
        },
        vertexShader: WaterShader.vertexShader,
        fragmentShader: WaterShader.fragmentShader
      });
      this.mesh = new THREE.Mesh(geometry, this.material);
      this.mesh.position.z = 0;
    }

    Water.prototype.getMesh = function() {
      return this.mesh;
    };

    Water.prototype.update = function(speed) {
      this.distance += speed;
      return this.material.uniforms.time.value = this.distance;
    };

    Water.prototype.resetPosition = function() {
      this.distance = 0.0;
      return this.material.uniforms.time.value = this.distance;
    };

    return Water;

  })();

  WaterShader = (function() {
    function WaterShader() {}

    WaterShader.uniforms = {
      time: {
        type: "f",
        value: 0.0
      }
    };

    WaterShader.vertexShader = "// pass UV coords to the fragment shader\nvarying vec2 vUv;\n\n// vertex shader main\nvoid main() {\n\n  // dump the UV coords into the varying (for the fragment shader to use)\n  vUv = uv;\n\n  // modify vertix position based on terrain height\n  gl_Position = projectionMatrix * modelViewMatrix * vec4( position.xyz, 1.0 );\n}";

    WaterShader.fragmentShader = "// UV coords from the vertex shader\nvarying vec2 vUv;\n\n// animation ticker\nuniform float time;\n\nhighp float rand(vec2 co)\n{\n  highp float a = 12.9898;\n  highp float b = 78.233;\n  highp float c = 43758.5453;\n  highp float dt= dot(co.xy ,vec2(a,b));\n  highp float sn= mod(dt,3.14);\n  return fract(sin(sn) * c);\n}\n\nfloat myRound(float a) {\n  return floor(a * 200.0)/200.0;\n}\n\n// fragment shader main\nvoid main() {\n  float sparkle = rand(vec2(myRound(vUv.x), myRound(vUv.y + time / 2.1)));\n  //float sparkle = rand(vUv);\n  float wake = 0.3;\n  if (sparkle < 0.01) {\n    wake = 0.9;\n  }\n\n  // output solid color\n  gl_FragColor = vec4(wake, wake + 0.5, 0.7, 1.0);\n}";

    return WaterShader;

  })();

  WaterShaderOld = (function() {
    function WaterShaderOld() {}

    WaterShaderOld.uniforms = {
      time: {
        type: "f",
        value: 0.0
      },
      location: {
        type: "3f",
        value: [0.5, 0.025, 0.0]
      }
    };

    WaterShaderOld.vertexShader = "// pass UV coords to the fragment shader\nvarying vec2 vUv;\n\n// position of boat\nuniform vec3 location;\n\n// vertex shader main\nvoid main() {\n\n  // dump the UV coords into the varying (for the fragment shader to use)\n  vUv = uv;\n\n  // do nothing, dump vertex to output\n  gl_Position = projectionMatrix * modelViewMatrix * vec4( position.xyz, 1.0 );\n}";

    WaterShaderOld.fragmentShader = "// UV coords from the vertex shader\nvarying vec2 vUv;\n\n// position of boat\nuniform vec3 location;\n\n// animation ticker\nuniform float time;\n\n// fragment shader main\nvoid main() {\n  float wake = 0.4;\n  float boatSplash = abs(vUv.x - location.x) + abs(vUv.y - location.y) * 10.0;\n\n  if (boatSplash < 0.05 && location.z <= 0.2) {\n    wake = 0.9;\n  }\n\n  // output solid color\n  gl_FragColor = vec4(wake, wake, 1.0, 1.0);\n}";

    return WaterShaderOld;

  })();

  BoatState = (function() {
    BoatState.prototype.context = null;

    BoatState.prototype.angle = 0;

    BoatState.prototype.delta = 0;

    BoatState.prototype.height = 0;

    BoatState.prototype.speed = 0.001;

    BoatState.prototype.turnRate = 0.0;

    function BoatState(prevState) {
      if (prevState != null) {
        this.context = prevState.context;
        this.angle = prevState.angle;
        this.delta = prevState.delta;
        this.height = prevState.height;
        this.speed = prevState.speed;
      }
    }

    BoatState.prototype.collide = function(hit) {};

    BoatState.prototype.getSpeed = function() {};

    BoatState.prototype.turnLeft = function() {
      return this.angle += this.turnRate;
    };

    BoatState.prototype.turnRight = function() {
      return this.angle -= this.turnRate;
    };

    BoatState.prototype.turnToAngle = function(targetAngle) {
      var diff;
      diff = Math.max(Math.min(targetAngle - this.angle, this.turnRate), -this.turnRate);
      return this.angle += diff;
    };

    BoatState.prototype.reset = function() {
      this.angle = 0;
      this.delta = 0;
      this.height = 0;
      return this.speed = 0.001;
    };

    return BoatState;

  })();

  AirState = (function(_super) {
    __extends(AirState, _super);

    AirState.prototype.GRAVITY = 0.005;

    function AirState(prevState) {
      AirState.__super__.constructor.call(this, prevState);
      this.turnRate = 0.001;
    }

    AirState.prototype.collide = function(hit) {
      this.delta -= this.GRAVITY;
      this.height += this.delta * 10;
      if (this.height / 10 < hit) {
        this.height = hit * 10;
        this.context.state = hit < 0.2 ? (this.delta = 0, new WaterState(this)) : (this.delta = (-this.delta * 0.3) - this.GRAVITY, new GroundState(this));
      }
    };

    AirState.prototype.getSpeed = function() {
      return this.speed = Math.max(this.speed - 0.000001, 0.000);
    };

    return AirState;

  })(BoatState);

  GroundState = (function(_super) {
    __extends(GroundState, _super);

    GroundState.prototype.prevHit = 0;

    GroundState.prototype.prevDelta = 0;

    function GroundState(prevState) {
      GroundState.__super__.constructor.call(this, prevState);
      this.turnRate = 0.0;
    }

    GroundState.prototype.collide = function(hit) {
      if (hit < 0.2) {
        this.context.state = new WaterState(this);
        return;
      }
      if (this.prevHit === 0) {
        this.prevHit = hit;
        return;
      }
      this.prevDelta = this.delta;
      this.delta = hit - this.prevHit;
      if (this.delta < this.prevDelta) {
        this.delta = this.prevDelta;
        this.height = (this.prevHit + this.prevDelta) * 10;
        this.context.state = new AirState(this);
        return;
      } else {
        this.height = hit * 10;
      }
      this.prevHit = hit;
    };

    GroundState.prototype.getSpeed = function() {
      return this.speed = Math.max(this.speed - 0.0001, 0.000);
    };

    return GroundState;

  })(BoatState);

  WaterState = (function(_super) {
    __extends(WaterState, _super);

    function WaterState(prevState) {
      WaterState.__super__.constructor.call(this, prevState);
      this.height = 0;
      this.turnRate = 0.02;
    }

    WaterState.prototype.collide = function(hit) {
      if (hit > 0.2) {
        this.context.state = new GroundState(this);
      }
    };

    WaterState.prototype.getSpeed = function() {
      return this.speed = Math.min(this.speed + 0.00001, 0.01);
    };

    return WaterState;

  })(BoatState);

}).call(this);
