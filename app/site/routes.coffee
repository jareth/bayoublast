express = require 'express'

index = (req, res)->
  res.render 'index',
    title: 'BayouBlast',
    page:  'index'

module.exports = (app) ->
  app.get '/', index