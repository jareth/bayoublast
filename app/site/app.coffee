# General page functions
$ ->
  initMenuEvents()
  initKeyboardEvents()
  
initMenuEvents = ->
  $('#toggleMenu').click toggleMenu
    
  $('#resetStreams').click ->
    window.app.resetPosition()
    
  $('#seedGenerate').click ->
    window.app.generate $('#seedEntry').val()
    
  $('#switchCamera').click ->
    window.app.switchCamera()
    
  $('#arcadeMode').click ->
    try
      $('#start').toggle(false)
      $container = $('#container')
      $container.toggle(true)
      window.app = new Main($container.get(0), $('#timer').get(0))
      window.app.animate(0)
    catch error
      console.log('Creating 3d scene failed.', error)
    
  $('#settings').click ->
    $('#mainMenu').toggle(false)
    $('#settingsMenu').toggle(true)
    changeSelection $('#settingsMenu').children(':first')
    
    
  $('#closeSettings').click ->
    $('#mainMenu').toggle(true)
    $('#settingsMenu').toggle(false)
    changeSelection $('#settings').parent()
    
initKeyboardEvents = ->
  $(document).keydown (event) ->
    switch event.which
      when 27 then toggleMenu()
      when 38 then selectionUp()
      when 40 then selectionDown()
      when 13, 32 then choose()
      else console.log event.which
    
toggleMenu = ->
  isOpening = !$('#settingsMenu').is(':visible')
  $('#start').toggle(isOpening)
  $('#mainMenu').toggle(!isOpening)
  $('#settingsMenu').toggle(isOpening)
  window.app.togglePause(isOpening)
  
selectionUp = ->
  $current = $('.menu .selected')
  $next = if $current.prev().length != 0 then $current.prev() else $current.siblings(':last')
  changeSelection $next
  
selectionDown = ->
  $current = $('.menu .selected')
  $prev = if $current.next().length != 0 then $current.next() else $current.siblings(':first')
  changeSelection $prev
  
choose = ->
  $current = $('.menu .selected')
  $current.children('a').click()
  
changeSelection = ($target)->
  $current = $('.menu .selected')
  $current.removeClass 'selected'
  $target.addClass 'selected'
  