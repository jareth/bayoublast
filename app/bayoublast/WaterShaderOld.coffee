# Water shader object
#
class WaterShaderOld
  @uniforms:
    
    time:
      type: "f"
      value: 0.0
    
    location:
      type: "3f"
      value: [0.5, 0.025, 0.0]

  @vertexShader:
    """
    // pass UV coords to the fragment shader
    varying vec2 vUv;
    
    // position of boat
    uniform vec3 location;
    
    // vertex shader main
    void main() {

      // dump the UV coords into the varying (for the fragment shader to use)
      vUv = uv;

      // do nothing, dump vertex to output
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position.xyz, 1.0 );
    }
    """

  @fragmentShader:
    """
    // UV coords from the vertex shader
    varying vec2 vUv;
    
    // position of boat
    uniform vec3 location;

    // animation ticker
    uniform float time;

    // fragment shader main
    void main() {
      float wake = 0.4;
      float boatSplash = abs(vUv.x - location.x) + abs(vUv.y - location.y) * 10.0;
    
      if (boatSplash < 0.05 && location.z <= 0.2) {
        wake = 0.9;
      }

      // output solid color
      gl_FragColor = vec4(wake, wake, 1.0, 1.0);
    }
    """