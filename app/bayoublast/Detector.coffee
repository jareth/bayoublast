# Browser capabilities detection
#
# @property canvas [Boolean] browser has canvas support
#
# @property webgl [Boolean] browser has webgl support
#
# @property workers [Boolean] browser supports workers
#
# @property fileapi [Boolean] browser supports fileapi
#
# @property deviceMotion [Boolean] browser supports device motion event
#
# @property deviceMotion [Boolean] browser supports device orientation event
#
class Detector

  # [Boolean] browser has canvas support
  @canvas: window.CanvasRenderingContext2D?

  # [Boolean] browser has webgl support
  @webgl: (
    try
      canvas = document.createElement 'canvas'

      # if has render context AND
      # if context IS EITHER webgl OR experimental-webgl
      true if window.WebGLRenderingContext? and (
        (canvas.getContext 'webgl')? or
        (canvas.getContext 'experimental-webgl')?
      )

    catch error
      false
  )

  # [Boolean] browser supports workers
  @workers: !! window.Worker

  # [Boolean] browser supports fileapi
  @fileapi = (window.File? and
    window.FileReader? and
    window.FileList? and
    window.Blob?)
    
  # [Boolean] browser supports device motion event
  @deviceMotion: !! window.DeviceMotionEvent
    
  # [Boolean] browser supports device orientation event
  @deviceOrientation: !! window.DeviceOrientationEvent