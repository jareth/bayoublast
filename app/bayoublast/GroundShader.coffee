# Ground shader object
#
class GroundShader
  @uniforms:

    time:
      type: "f"
      value: 0.0

    scale:
      type: "f"
      value: 1.0


  @vertexShader:
    """
    // pass UV coords to the fragment shader
    varying vec2 vUv;

    // pass height to fragment shader
    varying float fHeight;

    // height of landscape
    uniform float scale;

    // animation ticker
    uniform float time;

    // stream uniforms
    uniform vec4 streams[10];
    uniform int streamCount;

    // stream1

    // calculate ground height for position: 0 = water, 1 = land
    float renderStream( vec2 p, float freq, float phase, float amp, float xOff) {
      float h = 0.0;
      float s1 = cos( (p.y + phase + time) * freq ) / amp + 0.5 - xOff;
      float w1 = abs(( cos( (p.y + phase + time) * (freq / 7.0)) + 0.5) / (amp * 2.0));
      if (p.x > s1) {
        h = p.x - (s1 + w1);
      } else if (p.x < s1) {
        h = (s1 - w1) - p.x;
      }
      h = min(max(h * 20.0, 0.0) , 1.0);
      return h;
    }

    float renderMap( vec2 p ) {
      float h = 1.0;
      for (int iii = 0; iii < 10; iii++) {
        if (iii < streamCount) {
          h *= renderStream( p, streams[iii].r, streams[iii].g, streams[iii].b, streams[iii].a);
        }
      }
      return h;
    }

    // vertex shader main
    void main() {

      // dump the UV coords into the varying (for the fragment shader to use)
      vUv = uv;

      // calculate terrain height
      fHeight = renderMap(vUv);

      // modify vertix position based on terrain height
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position.xy, fHeight * scale, 1.0 );
    }
    """

  @fragmentShader:
    """
    // UV coords from the vertex shader
    varying vec2 vUv;

    // height from vertex shader
    varying float fHeight;

    // animation ticker
    uniform float time;

    // fragment shader main
    void main() {

      // output color blend based on height
      gl_FragColor = mix(
        mix(
          vec4(0.2, 0.9, 0.7, 1.0),
          vec4(0.7, 0.5 - fHeight * 0.3, 0.3 - fHeight * 0.2, 1.0),
          smoothstep(fract((vUv.y + time) * 20.0), 0.0, 0.005)
        ),
        vec4(0.2, 0.9, 0.7, 1.0),
        step(0.99, fract(vUv.x * 10.0))
      );
      gl_FragColor.a = floor(fHeight + 0.8);
    }
    """