# Holds water plane mesh data and logic to update
#
class Water

  distance: 0.0

  # Construct a new water object.
  #
  # @param width [int] water width
  # @param length [int] water length
  # @param color [uint] water color
  #
  constructor: (width, length, color) ->
    geometry = new THREE.PlaneBufferGeometry width, length, 100, 100

    # @material = new THREE.MeshBasicMaterial color: 0x6666FF
    @material = new THREE.ShaderMaterial
      uniforms:
        time:
          type: "f"
          value: 0.0
      vertexShader: WaterShader.vertexShader
      fragmentShader: WaterShader.fragmentShader
    # @material.wireframe = true

    @mesh = new THREE.Mesh geometry, @material
    @mesh.position.z = 0

  # Get water mesh object
  #
  getMesh: -> @mesh

  # update the ground rendering based on speed
  #
  # @params speed [float] speed (distance moved per frame)
  #
  update: (speed) ->
    @distance += speed
    @material.uniforms.time.value = @distance
    
  resetPosition: () ->
    @distance = 0.0
    @material.uniforms.time.value = @distance
