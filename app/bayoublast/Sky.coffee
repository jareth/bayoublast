# Show skyand sunset
#
class Sky

  distance: 0.0
  stepper: 0.0

  # Construct a new sky object.
  #
  # @param width [int] sky width
  # @param length [int] sky length
  # @param color [uint] sky color
  #
  constructor: (width, length) ->
    geometry = new THREE.PlaneBufferGeometry width, length, 100, 100

    # @texture = THREE.ImageUtils.loadTexture('img/sunset.png')
    # @material = new THREE.MeshBasicMaterial color: 0xFFDDAA, map: @texture
    @material = new THREE.ShaderMaterial
      uniforms:
        time:
          type: "f"
          value: 0.0
      vertexShader: SkyShader.vertexShader
      fragmentShader: SkyShader.fragmentShader
    # @material.wireframe = true

    @mesh = new THREE.Mesh geometry, @material
    @mesh.position.z = 0
    @mesh.rotation.x = Math.PI / 2

  # Get sky mesh object
  #
  getMesh: -> @mesh
  
  update: (speed) ->
    @stepper += 1
    if @stepper % 30 is 0
      @distance += speed
      @material.uniforms.time.value = @distance
