# Water shader object
#
class WaterShader
  @uniforms:

    time:
      type: "f"
      value: 0.0

  @vertexShader:
    """
    // pass UV coords to the fragment shader
    varying vec2 vUv;

    // vertex shader main
    void main() {

      // dump the UV coords into the varying (for the fragment shader to use)
      vUv = uv;

      // modify vertix position based on terrain height
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position.xyz, 1.0 );
    }
    """
    
  @fragmentShader:
    """
    // UV coords from the vertex shader
    varying vec2 vUv;

    // animation ticker
    uniform float time;
    
    highp float rand(vec2 co)
    {
      highp float a = 12.9898;
      highp float b = 78.233;
      highp float c = 43758.5453;
      highp float dt= dot(co.xy ,vec2(a,b));
      highp float sn= mod(dt,3.14);
      return fract(sin(sn) * c);
    }
    
    float myRound(float a) {
      return floor(a * 200.0)/200.0;
    }

    // fragment shader main
    void main() {
      float sparkle = rand(vec2(myRound(vUv.x), myRound(vUv.y + time / 2.1)));
      //float sparkle = rand(vUv);
      float wake = 0.3;
      if (sparkle < 0.01) {
        wake = 0.9;
      }

      // output solid color
      gl_FragColor = vec4(wake, wake + 0.5, 0.7, 1.0);
    }
    """