# Main class for Bayou Blast game
#
# @example How to create
#   window.app = new Main(document.getElementById('container'));
#     window.app.animate();
#
class @Main

  # debug counter to stop animation loop after x cycles
  distance: 0.0
  isPaused: false
  isFpsCamera: true
  currentOrientation: -1
  lastTime: 0.0
  remainingTime: 30000.0
  nextCheckpoint: 6

  # Construct a new Main
  #
  # @param domElement [Element] parent HTML node to render scene in.
  #
  constructor: (domElement, @timerElement) ->

    if not Detector.webgl
       #show no support message
      domElement.innerHTML = '<p> WebGL not supported. </p>'

       #stop animate from triggering render (as nothing is setup)
      @animate = () ->
        console.log "animate was overriden because WebGL/Canvas not supported."

       #and fail creation
      throw new Error "WebGL or Canvas not supported."

    # stats - remove on prod
    @stats = new Stats()
    @stats.setMode( 1 )
    document.body.appendChild @stats.domElement
    
    @scene = new THREE.Scene()
    @camera = new THREE.PerspectiveCamera(
      100,
      domElement.clientWidth / domElement.clientHeight,
      0.1,
      10000
    )

    @renderer = new THREE.WebGLRenderer()
    @renderer.setSize domElement.clientWidth, domElement.clientHeight
    # @renderer.setClearColor 0x99b233, 1

    # cleanup domElement and add Three.js window
    domElement.removeChild(domElement.firstChild) while (domElement.firstChild)
    domElement.appendChild @renderer.domElement

    @streams = new Streams Date(), 5
    
    # add elements
    @sky = new Sky 17000, 4000
    @sky.getMesh().position.y = 4000
    @sky.getMesh().position.z = 1000
    @scene.add @sky.getMesh()

    @ground = new Ground 500, 2000, @streams, 50
    @ground.getMesh().position.y = 900
    @scene.add @ground.getMesh()

    @farground = new Ground 500, 2000, @streams, 30
    @farground.getMesh().position.y = 2900
    @scene.add @farground.getMesh()
    @farground.update 1

    @water = new Water 500, 4000, 0x0000FF
    @water.getMesh().position.y = 1900
    @water.getMesh().position.z = -5
    @scene.add @water.getMesh()

    # boat adds it's own mesh because it needs to wait on the model loading
    # so we pass it the @scene to add itself to
    @boat = new Boat(@scene)
    @boat.resetPosition(@streams.getCenterOfWidestStream() * 500 - 250)

    # add lights
    light = new THREE.PointLight 0xffffff
    light.position.set -100,200,100
    @scene.add light

    ambientLight = new THREE.AmbientLight 0x111111
    @scene.add ambientLight

    # set camera
    @camera.position.y = -40
    @camera.position.z = 150
    @camera.lookAt new THREE.Vector3 0, 50, 0

    # keyboard controls
    @keyboard = new THREEx.KeyboardState()

    # listen for resize
    @domElement = domElement
    window.addEventListener 'resize', @onWindowResize, false
    @onWindowResize()
    
    if Detector.deviceOrientation
      console.log 'orientation supported'
      window.addEventListener 'deviceorientation', @deviceOrientationHandler, false

  # Render the three.js scene. Typically only called from the animation loop.
  #
  render: () ->
    @scene.updateMatrixWorld()
    @renderer.clear()

    @renderer.render @scene, @camera

  # Correct the scene size and aspect ratio on window resize.
  #
  onWindowResize: () =>
    @camera.aspect = @domElement.clientWidth / @domElement.clientHeight
    @camera.updateProjectionMatrix()
    @renderer.setSize @domElement.clientWidth, @domElement.clientHeight

  # Main animation loop. continually requests animation loop for itself.
  #
  animate: (thisTime) =>
    if @isPaused or @remainingTime <= 0
      @lastTime = thisTime
      requestAnimationFrame(@animate)
      return
      
    if @distance > @nextCheckpoint
      @nextCheckpoint += 6
      @remainingTime += 10000
    
    if @lastTime is 0
      @lastTime = thisTime
    else
      timeDelta = thisTime - @lastTime
      @remainingTime -= timeDelta
      @lastTime = thisTime
      @timerElement.innerHTML = @pad(Math.ceil(@remainingTime * 0.001), 2, '0')

    # stats
    @stats.begin()

    # update boat speed - boat states handle acceleration
    speed = @boat.getSpeed()
    @isPaused = speed is 0

    # try turning - boat states handle turning rate
    if @keyboard.pressed "left"
      # move boat left
      @boat.turnLeft()
    if @keyboard.pressed "right"
      # move boat right
      @boat.turnRight()
    if @currentOrientation != -1
      rotValue = 0
      if @currentOrientation is 0
        rotValue = @gamma
      else if @currentOrientation is 90
        rotValue = @beta
      else if @currentOrientation is -90
        rotValue = -@beta
      if rotValue < -1 or rotValue > 1
        targetAngle = -rotValue * Math.PI / 180
        @boat.turnToAngle targetAngle

    # update elements based on new speed
    @distance += speed
    @streams.update(speed)
    @ground.update(speed)
    @farground.update(speed)
    @water.update(speed)
    @boat.update(speed)
    @sky.update(speed)

    ################
    # Hit detection

    #get boat position
    point = @boat.getPosition()

    #adjust position to mesh coords (0,0 bottom left 1,1 top right)
    point.x = (point.x + 250) / 500
    point.y = 100 / 2000

    #check height of ground
    height = @streams.checkCollision point

    #update boat state
    @boat.collide(height)

    # End hit detection
    ################
    
    #@water.update point.x, @boat.state.height
    
    if @isFpsCamera
      # camera mode - inside boat
      @camera.position.y = -10
      @camera.position.z = 20 + @boat.getPosition().z + Math.sin(@distance)
    else
      # camera mode - chase
      @camera.position.y = -40
      @camera.position.z = 150 - (speed * 3000)
    @camera.position.x = @boat.getPosition().x
    @camera.lookAt new THREE.Vector3 @boat.getPosition().x, 50, @boat.getPosition().z

    @render()

    #if @counter < 1000
    requestAnimationFrame(@animate)
      #@counter++

    # stats
    @stats.end()
    
  togglePause: (isPausing) ->
    @isPaused = isPausing
    
  resetPosition: () ->
    @distance = 0.0
    @streams.resetPosition()
    @ground.resetPosition()
    @farground.resetPosition()
    @boat.resetPosition(@streams.getCenterOfWidestStream() * 500 - 250)
    @farground.update 1
    @lastTime = 0
    @remainingTime = 30000.0
    @nextCheckpoint = 6
    
    @render()
    
  generate: ( seed ) ->
    @streams.generate seed, 5
    @resetPosition()
    
  deviceOrientationHandler: (eventData) =>
    @gamma = eventData.gamma
    @beta = eventData.beta
    @currentOrientation = window.orientation
    
  switchCamera: () ->
    @isFpsCamera = !@isFpsCamera
    
  pad: (val, length, padChar = '0') ->
    val += ''
    numPads = length - val.length
    if (numPads > 0) then new Array(numPads + 1).join(padChar) + val else val