# Contains logic to initialise x number of streams.
# Contains logic to perform collision detection against streams.
#
class Streams

  distance: 0.0

  # Construct a new streams object.
  #
  # @param width [int] ground width
  # @param length [int] ground length
  #
  constructor: ( seed, @num ) ->
    @generate seed, num

  generate: ( seed, @num ) ->
    # setup streams
    @streams = []
    #prng = new Math.seedrandom seed
    Math.seedrandom seed
    for iii in [1..num]
      # float freq, float phase, float amp, float xOff
      @streams.push new THREE.Vector4(
        (Math.random() * 4 ) + 3,
        (Math.random() * 10) + 0,
        (Math.random() * 2 ) + 10,
        (Math.random() * 0.6) - 0.3
        #(0.6 / num * iii) - 0.3
      )

  # array of stream values
  getUniformArray: ->
    return @streams

  # number of streams used
  getStreamCount: ->
    return @num

  # update the stream rendering based on speed
  #
  # @params speed [float] speed (distance moved per frame)
  #
  update: (speed) ->
    @distance += speed
    return

  # cycle through all streams and check collision height
  #
  # @params point [THREE.Vector2] point to check in xy plane
  #
  # @return [float] height at position: water = 0, ground > 0
  #
  checkCollision: (point) ->
    height = 1.0
    for stream in @streams
      height *= @getHeight point, stream.x, stream.y, stream.z, stream.w
    return height

  # javascript equivalent of renderStream function in shader
  #
  # @params point [THREE.Vector2] position in xy plane to calculate height for
  # @params freq [float] frequency of stream
  # @params phase [float] phase offset of stream
  # @params amp [float] amplitude of stream
  # @params xOff [float] x offset of stream
  #
  # @return [float] height at position: water = 0, ground > 0
  #
  getHeight: (point, freq, phase, amp, xOff) ->
    height = 1.0
    center = Math.cos( (point.y + phase + @distance) * freq ) / amp + 0.5 - xOff
    width = Math.abs(( Math.cos( (point.y + phase + @distance) * (freq / 7.0)) + 0.5) / (amp * 2.0))
    if (point.x > center)
      height = point.x - (center + width)
    else if (point.x < center)
      height = (center - width) - point.x
    height = Math.min(Math.max(height * 20.0, 0.0) , 1.0)
    return height
    
  resetPosition: () ->
    @distance = 0.0
    
  getCenterOfWidestStream: () ->
    widest = 0
    center = 0.5
    for stream in @streams
      centerAndWidth = @getWidth stream.x, stream.y, stream.z, stream.w
      console.log centerAndWidth
      if centerAndWidth[1] > widest
        widest = centerAndWidth[1]
        center = centerAndWidth[0]
    return center
      
  getWidth: (freq, phase, amp, xOff) ->
    center = Math.cos( (phase + @distance) * freq ) / amp + 0.5 - xOff
    width = Math.abs(( Math.cos((phase + @distance) * (freq / 7.0)) + 0.5) / (amp * 2.0))
    return [center, width]
    
    
