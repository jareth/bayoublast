# Sky shader object
#
class SkyShader
  @uniforms:

    time:
      type: "f"
      value: 0.0

  @vertexShader:
    """
    // pass UV coords to the fragment shader
    varying vec2 vUv;

    // vertex shader main
    void main() {

      // dump the UV coords into the varying (for the fragment shader to use)
      vUv = uv;

      // modify vertix position based on terrain height
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position.xyz, 1.0 );
    }
    """
    
  @fragmentShader:
    """
    // UV coords from the vertex shader
    varying vec2 vUv;

    // animation ticker
    uniform float time;
    
    highp float rand(vec2 co)
    {
      highp float a = 12.9898;
      highp float b = 78.233;
      highp float c = 43758.5453;
      highp float dt= dot(co.xy ,vec2(a,b));
      highp float sn= mod(dt,3.14);
      return fract(sin(sn) * c);
    }
    
    float myRound(float a) {
      return floor(a * 100.0)/100.0;
    }

    // fragment shader main
    void main() {
      float sparkle = rand(vec2(myRound(vUv.x), myRound(vUv.y - time * 20.0))) * 0.05;
      vec2 center = vUv - vec2(0.5, 0.25);
      float dist = 1.0 - sqrt(dot(center, center));

      // output solid color
      gl_FragColor = vec4(dist + sparkle, max(dist - 0.6, sparkle) * 1.8, max(0.9 - sparkle - dist, 0.0) * 2.0, 1.0);
      // gl_FragColor = mix(
      //  vec4(sparkle, 0.0, 0.0, 1.0),
      //  vec4(1.0, 1.0, 1.0, 1.0),
      //  step(dist, 0.1)
      //);
    }
    """