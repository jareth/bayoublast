# Class represents boat colliding with ground.
# Boat is decelerating and cannot turn.
#
class GroundState extends BoatState

  # ground height during previous hit cycle
  prevHit: 0

  # change of ground height during previous hit cycle
  prevDelta: 0

  # Construct a new Ground State.
  #
  # @param prevState [BoatState] previous state to inherit properties from
  #
  constructor: (prevState) ->
    super prevState
    @turnRate = 0.0

  # Triggered from animation loop and passes current ground height
  #
  # Continually calculates delta from last hit cycle.
  # Delta becomes vertical speed and is used to launch into airstate.
  #
  # @param hit [float] current ground height: water = 0, ground > 0
  #
  collide: (hit) ->
    if hit < 0.2
      # no longer colliding with ground - switch back to water state
      @context.state = new WaterState this
      return
      
    if @prevHit is 0
      # PrevHit is 0 if this is the first collide call since changing state.
      # We don't have enough data to calculate anything, so store the hit
      # and exit.
      @prevHit = hit
      return

    @prevDelta = @delta
    # change in height = previous height - current height
    @delta = hit - @prevHit

    if @delta < @prevDelta
      # The new delta is less than the old delta - the boat should launch into
      # the AirState
      @delta = @prevDelta
      @height = (@prevHit + @prevDelta ) * 10
      @context.state = new AirState this
      return

    else
      # delta is greater than previous delta, the boat is going up a slope.
      @height = hit * 10

    # staying in GroundState - store prev hit for next loop
    @prevHit = hit

    return

  # Returns the current boat speed minus deceleration
  #
  # @return [float] current boat speed
  #
  getSpeed: ->
    @speed = Math.max(@speed - 0.0001, 0.000)