# Base class for all BoatStates (player states)
#
# ** ABSTRACT CLASS ** DO NOT INSTANTIATE
#
# @example How to subclass BoatState
#   class MyState extends BoatState
#     constructor: (prevState) ->
#       super prevState
#
class BoatState

  # Stores the context to allow states to change themselves.
  #
  # @example Change states
  #   @context.state = new GroundState this
  #
  context: null

  # Boat angle in radians - 0 straight up
  angle: 0

  # Change in ground height since last update - used for collision detection and jump / bounce physics
  delta: 0

  # Height of boat from water level - used for 'jumping' boat
  height: 0

  # Speed of boat
  speed: 0.001
  
  # the amonut the boat is allowed to turn in the current state, used in the turnLeft, turnRight and turnToAngle functions
  turnRate: 0.0

  # Set of standard operations when constructing a new state
  #
  # @param prevState [BoatState] previous state to inherit properties from
  #
  constructor: (prevState) ->
    if prevState?
      @context = prevState.context

      @angle = prevState.angle
      @delta = prevState.delta
      @height = prevState.height
      @speed = prevState.speed

  # Triggered from animation loop and passes current ground height
  #
  # @param hit [float] current ground height: water = 0, ground > 0
  #
  collide: (hit) ->

  # Returns the current boat speed (including acceleration adjustments)
  #
  # @return [float] current boat speed
  #
  getSpeed: ->

  # Turn boat right by predefiend amount. Call repeatedly to turn further.
  turnLeft: ->
    @angle += @turnRate

  # Turn boat left by predefiend amount. Call repeatedly to turn further.
  turnRight: ->
    @angle -= @turnRate
    
  # Turn the boat as much as possible towards the targetAngle, limited by the turnRate
  turnToAngle: (targetAngle) ->
    diff = Math.max(Math.min(targetAngle - @angle, @turnRate), -@turnRate)
    @angle += diff
    
  # Reset preoperties to initial values, for restarting the game
  reset: ->
    @angle = 0
    @delta = 0
    @height = 0
    @speed = 0.001