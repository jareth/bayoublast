# Class represents boat 'jumping' thorugh air.
# Boat is slightly decelerating and can turn very slightly.
#
class AirState extends BoatState
  
  # gravity acceleration
  GRAVITY: 0.005

  # Construct a new Air State.
  #
  # @param prevState [BoatState] previous state to inherit properties from
  #
  constructor: (prevState) ->
    super prevState
    @turnRate = 0.001

  # Triggered from animation loop and passes current ground height
  #
  # @param hit [float] current ground height: water = 0, ground > 0
  #
  collide: (hit) ->
    # add gravity to @delta
    @delta -= @GRAVITY

    # set new height based on gravity adjusted @delta
    @height += @delta * 10

    if @height / 10 < hit
      # the boat height is now less than the collision height - has 'landed'

      # reset boat height to collision height
      @height = hit * 10

      @context.state = if hit < 0.2
        #reset delta - no bounce in water
        @delta = 0
      
        # 'landed' in water - go to water state
        new WaterState this
      else
        # reverse delta and reduce to make 'bounce'
        @delta = (-@delta * 0.3) - @GRAVITY
        
        # 'landed' on ground - go to ground state
        new GroundState this

    return

  # Returns the current boat speed minus air friction deceleration
  #
  # @return [float] current boat speed
  #
  getSpeed: ->
    @speed = Math.max(@speed - 0.000001, 0.000)
