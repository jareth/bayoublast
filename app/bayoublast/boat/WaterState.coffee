# Class represents boat in water. Boat can accelerate and turn.
#
class WaterState extends BoatState

  # Construct a new Water State.
  # Sets @height to 0
  #
  # @param prevState [BoatState] previous state to inherit properties from
  #
  constructor: (prevState) ->
    super prevState
    @height = 0
    @turnRate = 0.02

  # Triggered from animation loop and passes current ground height
  #
  # @param hit [float] current ground height: water = 0, ground > 0
  #
  collide: (hit) ->
    if hit > 0.2
      @context.state = new GroundState this
    return

  # Returns the current boat speed plus acceleration
  #
  # @return [float] current boat speed
  #
  getSpeed: ->
    @speed = Math.min(@speed + 0.00001, 0.01)
