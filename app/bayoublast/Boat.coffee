# Boat class holds boat mesh data and logic for turning and acceleration.
#
class Boat

  state: null

  # Construct a new boat object.
  #
  # @params scene [THREE.Scene] boat will add itself to this after loading mesh
  #
  constructor: (scene) ->
    @state = new WaterState null
    @state.context = this
    @position = new THREE.Vector3 0, 0, 0

    loader = new THREE.JSONLoader()

    #load model
    loader.load '/js/boat1.js', (geometry, materials) =>

      #@material = new THREE.MeshBasicMaterial color: 0xFF0000
      #@material.wireframe = true
      @material = new THREE.MeshFaceMaterial( materials )

      @mesh = new THREE.Mesh geometry, @material

      # set scale
      @mesh.scale.set 0.4, 0.4, 0.4
      # correct rotation
      @mesh.rotation.x = Math.PI / 2
      @mesh.rotation.y = Math.PI / 2

      # postion
      @mesh.position.z = @position.clone()

      scene.add @mesh

  # Get boat mesh object
  #
  # @return [THREE.Mesh]
  #
  getMesh: -> @mesh

  # Move the boat horizontally based on speed and angle.
  # Move vertically based on height.
  #
  # @params speed [float] speed of boat (distance moved per frame)
  #
  update: (speed) ->
    #@material.uniforms.time.value += speed
    @position.x -= (@state.angle * 1500 * @state.speed)
    @position.z = 10 + @state.height
    if @mesh?
      @mesh.position.x = @position.x
      @mesh.position.z = @position.z
      @mesh.rotation.y = Math.PI / 2 + @state.angle
    return

  # get position of boat mesh, or return 0,0 if mesh not loaded yet
  #
  # #return [THREE.Vector3] position of boat (in xy plane)
  #
  getPosition: ->
    return @position.clone()

  # Returns boat speed
  #
  # @return [float] boat speed
  #
  getSpeed: ->
    @state.getSpeed()

  # Turn boat left by predefiend amount. Call repeatedly to turn further.
  turnLeft: ->
    @state.turnLeft()

  # Turn boat right by predefiend amount. Call repeatedly to turn further.
  turnRight: ->
    @state.turnRight()

  # Triggered from animation loop and passes current ground height
  #
  # @param hit [float] current ground height: water = 0, ground > 0
  #
  collide: (hit) ->
    @state.collide hit
    
    #if @mesh?
      #if @state instanceof WaterState
        #@material.color.setHex 0x00FF00
      #else if @state instanceof GroundState
        #@material.color.setHex 0xFF0000
      #else if @state instanceof AirState
        #@material.color.setHex 0x0000FF
    
  resetPosition: (widest) ->
    @position = new THREE.Vector3 widest, 0, 0
    if mesh? then @mesh.position = @position.clone()
    @state.reset()
    
  turnToAngle: (targetAngle) ->
    @state.turnToAngle targetAngle

