# Holds ground plane mesh data and logic to update
#
class Ground

  distance: 0.0

  # Construct a new ground object.
  #
  # @param width [int] ground width
  # @param length [int] ground length
  #
  constructor: (width, length, @streams, density = 100) ->

    geometry = new THREE.PlaneBufferGeometry width, length, density, density

    @material = new THREE.ShaderMaterial
      uniforms:
        time:
          type: "f"
          value: 0.0
        scale:
          type: "f"
          value: 10.0
        streams:
          type: "v4v"
          value: streams.getUniformArray()
        streamCount:
          type: "i"
          value: streams.getStreamCount()
      vertexShader: GroundShader.vertexShader
      fragmentShader: GroundShader.fragmentShader
    @material.transparent = true
    # @material.wireframe = true

    @mesh = new THREE.Mesh geometry, @material

  # Get ground mesh object
  #
  getMesh: -> @mesh

  # update the ground rendering based on speed
  #
  # @params speed [float] speed (distance moved per frame)
  #
  update: (speed) ->
    @distance += speed
    @material.uniforms.time.value = @distance
    
  resetPosition: () ->
    @distance = 0.0
    @material.uniforms.time.value = @distance
    @material.uniforms.streams.value = @streams.getUniformArray()
